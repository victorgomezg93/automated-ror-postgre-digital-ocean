resource "digitalocean_droplet" "bastion"{

    image = var.image

    name = "bastion-${var.name}-${var.region}"

    region = var.region

    size = "s-1vcpu-1gb"

    ssh_keys = [digitalocean_ssh_key.default.fingerprint]

    vpc_uuid = digitalocean_vpc.web.id

    tags = ["${var.name}-webserver"]


    lifecycle {
        create_before_destroy = true
    }
}

resource "digitalocean_record" "bastion" {

    # Get the domain from our data source
    domain = data.digitalocean_domain.web.name

    # An A record is an IPv4 name record. Like www.digitalocean.com
    type   = "A"

    # Set the name to bastion-name-region
    name   = "bastion-${var.name}-${var.region}"

    # Point the record at the IP address of our bastion droplet
    value  = digitalocean_droplet.bastion.ipv4_address

    # The Time-to-Live for this record is 30 seconds. Then the cache invalidates
    ttl    = 3600
}

resource "digitalocean_firewall" "bastion" {
    
    # Human friendly name of the firewall
    name = "${var.name}-only-ssh-bastion"

    # Droplets to apply the firewall to
    droplet_ids = [digitalocean_droplet.bastion.id]

    #--------------------------------------------------------------------------#
    # Rules to allow only ssh both inbound from the public internet and only   #
    # allow outbout ssh traffic into the VPC network. Also allow ping just for #
    # ease of use inside the VPC as well.                                      #
    #--------------------------------------------------------------------------#
    inbound_rule {
        protocol = "tcp"
        port_range = "22"
        source_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
        protocol = "tcp"
        port_range = "22"
        destination_addresses = [digitalocean_vpc.web.ip_range]
    }

    outbound_rule {
        protocol = "icmp"
        destination_addresses = [digitalocean_vpc.web.ip_range]
    }
}