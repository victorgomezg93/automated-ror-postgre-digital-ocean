variable do_token {
    default = "63e6ad59820ecd3f0bfe4e160308ef0809b1e0249ec99821492ca24cdbbca4eb"
}

variable "subdomain"{
    type = string
    default = "lab"
}

variable "domain_name" {
    type = string
    default = "elteuregal.com"
}

variable region {
    type = string
    default = "sfo3"
}

variable droplet_count {
    type = number
    default = 3
}

variable "name" {
    type = string
    default = "FCM"
}

variable "image" {
    type = string
    default = "ubuntu-20-04-x64"
}

variable "droplet_size" {
    type = string
    default = "s-1vcpu-1gb"
}

#variable "ssh_key" {
#    type = string
#}


// DATABASE

variable "database_size" {
    type = string
    default = "db-s-1vcpu-1gb"
}
variable "db_count" {
    type = number
    default = 1
}

//NETWORK
variable "network" {
    type = string
    default = "192.168.43.0/24"
}

//gitlab

variable "gitlab_user" {
  description = "gitlab username"
  type        = string
  sensitive   = true
}

variable "gitlab_password" {
  description = "gitlab password"
  type        = string
  sensitive   = true
}