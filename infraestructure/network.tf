resource "digitalocean_vpc" "web" {

    name = "web-vpc-no-default"

    region = var.region

    ip_range = var.network

}