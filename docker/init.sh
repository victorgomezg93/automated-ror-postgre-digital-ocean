#!/bin/sh
chmod +x automated-ror-postgre-digital-ocean/docker/entrypoints/docker-entrypoint.sh
chmod +x  automated-ror-postgre-digital-ocean/docker/entrypoints/sidekiq-entrypoint.sh
fuser -k 80/tcp
cd automated-ror-postgre-digital-ocean/docker/
docker-compose up -d
docker-compose exec -T app bundle exec rake db:setup db:migrate

